# Instructions
1. Fork this repository using Bitbucket's web interface
2. Clone your fork, this copies all data to your local machine. **git clone https://*yourlogin*@bitbucket.org/*yourlogin*/ucll-vpw.**
3. In your local repository, create a new branch. **git branch *mybranch* **
4. Switch to this branch. **git checkout *mybranch* **
5. Tell git you want this local branch to also exist in the remote repository. **get branch -u origin/*mybranch* **
6. Write solution to some problem
7. Add the files to the staging area. **git add *file1* *file2* ...**
8. Commit the files to the local repository. **git commit -m "*Commit message*"**
9. Synchronize your local repository with your remote repository. **git push**
10. Using Bitbucket's web interface, go to this repository, create a pull request, and ask for your branch *yourlogin*/*mybranch* to be merged with fvogels/master
11. Wait for your pull request to be accepted.
12. Your work has been immortalized. Eternal glory and fame to you.